---
title: "Hugo Tailwind"
---

# Home Page
Laborum voluptate pariatur ex culpa magna nostrud est incididunt fugiat
pariatur do dolor ipsum enim. Consequat tempor do dolor eu. Non id id anim anim
excepteur excepteur pariatur nostrud qui irure ullamco.

## h2 Right Content
Paragraph Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Cras mattis consectetur purus sit amet fermentum. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.


<h2>h2 Right Content</h2>

Maecenas sed diam eget risus varius blandit sit amet non magna. Maecenas faucibus mollis interdum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id elit non mi porta gravida at eget metus. Donec id elit non mi porta gravida at eget metus. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum.

Nullam quis risus eget urna mollis ornare vel eu leo. Donec sed odio dui. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Nullam id dolor id nibh ultricies vehicula ut id elit. Nulla vitae elit libero, a pharetra augue. Donec sed odio dui.

## h2 Title

Donec id elit non mi porta gravida at eget metus. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.


### Alerts
{{< alert >}}
  This is the default alert. **Markdown** is supported alerts.
{{< /alert >}}

{{< alert type="primary" >}}
  This is a Primary alert. **Markdown** is supported alerts.
{{< /alert >}}

{{< alert type="success" >}}
  This is a Success alert. **Markdown** is supported alerts.
{{< /alert >}}

{{< alert type="warning" >}}
  ## This is a Warning alert. Markdown is supported alerts.

  Cras mattis consectetur purus sit amet fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Vestibulum id ligula porta felis euismod semper. Donec id elit non mi porta gravida at eget metus. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.
{{< /alert >}}

{{< alert type="danger" >}}
  <i class="fas fa-rocket-launch fa-2x"></i>

  This is a Danger alert. **Markdown** is supported alerts.
{{< /alert >}}

{{< alert type="info" >}}
  This is a Info alert. **Markdown** is supported alerts.
{{< /alert >}}

#### Badges
#### Default

{{< badge >}}
  default badge
{{< /badge >}}

{{< badge type="primary" >}}
  primary badge
{{< /badge >}}

{{< badge type="success" >}}
  success badge
{{< /badge >}}

{{< badge type="warning" >}}
  warning badge
{{< /badge >}}

{{< badge type="danger" >}}
  danger badge
{{< /badge >}}

{{< badge type="info" >}}
  info badge
{{< /badge >}}


#### Large
{{< badge size="large" >}}
  large default badge
{{< /badge >}}

{{< badge type="primary" size="large" >}}
  large primary badge
{{< /badge >}}

{{< badge type="success" size="large" >}}
  large success badge
{{< /badge >}}

{{< badge type="warning" size="large" >}}
  large warning badge
{{< /badge >}}

{{< badge type="danger" size="large" >}}
  large danger badge
{{< /badge >}}

{{< badge type="info" size="large" >}}
  large info badge
{{< /badge >}}


## Buttons

{{< button url="#" text="Default Button" i="far fa-arrow-right-long">}}

{{< button url="#" text="Light Button" type="light" i="far fa-arrow-right-long">}}

{{< button url="#" text="Primary Button" type="primary" i="far fa-arrow-right-long">}}

{{< button url="#" text="Success Button" type="success" i="far fa-arrow-right-long">}}

{{< button url="#" text="Warning Button" type="warning" i="far fa-arrow-right-long">}}

{{< button url="#" text="Danger Button" type="danger" i="far fa-arrow-right-long">}}

{{< button url="#" text="Info Button" type="info" i="far fa-arrow-right-long">}}

----

{{< codeblock "java" >}}
fun gB(bN: String?): WebDriver {
  val d: WebDriver = if (bN != null && bN == "chrome") {
    val o = ChromeOptions()
    HTTP_PROXY?.let { pU ->

    }
    ChromeDriverManager.getInstance().setup()
    ChromeDriver(o)
  } else {
    val o = FirefoxOptions()
    HTTP_PROXY?.let { pU ->
      val pF = FirefoxProfile()
      val p = Proxy()
    }
    FirefoxDriverManager.getInstance().setup()
    FirefoxDriver(o)
  }
  return d
}
{{< /codeblock >}}

## h2 Cards

### Basic Cards
{{< card-container count=1 >}}
  {{< card-basic >}}
  ## h2 Title
  Vestibulum id ligula porta felis euismod semper. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur blandit tempus porttitor. Curabitur blandit tempus porttitor. Maecenas faucibus mollis interdum. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.
  {{< /card-basic >}}
  
  {{< card-basic >}}
  ### h3 Title
  Maecenas faucibus mollis interdum. Vestibulum id ligula porta felis euismod semper. Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Maecenas sed diam eget risus varius blandit sit amet non magna.

  Donec id elit non mi porta gravida at eget metus. Donec id elit non mi porta gravida at eget metus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur. Aenean lacinia bibendum nulla sed consectetur.
  {{< /card-basic >}}

{{< /card-container >}}



### h3 Heading
Cras mattis consectetur purus sit amet fermentum. Maecenas sed diam eget risus varius blandit sit amet non magna. Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget metus.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ullamcorper nulla non metus auctor fringilla. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sed diam eget risus varius blandit sit amet non magna. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam.


#### h4 Heading
Cras mattis consectetur purus sit amet fermentum. Maecenas sed diam eget risus varius blandit sit amet non magna. Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at eget metus.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ullamcorper nulla non metus auctor fringilla. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean lacinia bibendum nulla sed consectetur.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sed diam eget risus varius blandit sit amet non magna. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam.