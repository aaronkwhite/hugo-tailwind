/** @type {import('tailwindcss').Config} */
const themeDir = __dirname + "/../../";
const colors = require("./colors");

export default {
  purge: {
    enabled: process.env.HUGO_ENVIRONMENT === "production",
    content: [
      "./**/*.html",
      "./**/*.md",
      "../content/**/*.md", // Changed to include all .md files in subdirectories
      "../layouts/**/*.html",
      "themes/hawkdocs-tailwind/layouts/**/*.html",
      // Add other file types where your classes are being used
    ],
    defaultExtractor: (content) => {
      let els = JSON.parse(content).htmlElements;
      return els.tags.concat(els.classes, els.ids);
    },
  },
  content: [
    "./**/*.html",
    "./**/*.md",
    "../content/*.md",
    "../layouts/**/*.html",
  ],
  darkMode: "class",
  theme: {
    fontFamily: {
      sans: [
        "Inter",
        "ui-sans-serif",
        "system-ui",
        "-apple-system",
        "BlinkMacSystemFont",
        "Segoe UI",
        "Roboto",
        "Helvetica Neue",
        "Arial",
        "Noto Sans",
        "sans-serif",
        "Apple Color Emoji",
        "Segoe UI Emoji",
        "Segoe UI Symbol",
        "Noto Color Emoji",
      ],
      monospace: [
        "ui-monospace",
        "SFMono-Regular",
        "Menlo",
        "Monaco",
        "Consolas",
        "Liberation Mono",
        "Courier New",
        "monospace",
      ],
    },
    colors: colors,
    fontSize: {
      xs: ["13px", { lineHeight: "18px", fontWeight: "400" }],
      sm: ["14px", { lineHeight: "20px", fontWeight: "400" }],
      base: ["16px", { lineHeight: "24px", fontWeight: "400" }],
      md: ["18px", { lineHeight: "28px", fontWeight: "400" }],
      lg: ["20px", { lineHeight: "30px", fontWeight: "400" }],
      xl: ["24px", { lineHeight: "34px", fontWeight: "400" }],
      "2xl": ["28px", { lineHeight: "40px", fontWeight: "400" }],
      "3xl": ["32px", { lineHeight: "48px", fontWeight: "400" }],
      "4xl": ["40px", { lineHeight: "60px", fontWeight: "400" }],
      "5xl": ["44px", { lineHeight: "66px", fontWeight: "400" }],
      "6xl": ["48px", { lineHeight: "72px", fontWeight: "400" }],
      "7xl": ["56px", { lineHeight: "80px", fontWeight: "400" }],
      "8xl": ["64px", { lineHeight: "90px", fontWeight: "400" }],
      "9xl": ["72px", { lineHeight: "100px", fontWeight: "400" }],
    },
    animation: {},
    keyframes: {},
    extend: {
      width: {
        16: "16px",
        24: "24px",
        32: "32px",
        40: "40px",
        48: "48px",
        64: "64px",
        96: "96px",
        128: "128px",
      },
      height: {
        16: "16px",
        24: "24px",
        32: "32px",
        40: "40px",
        48: "48px",
        64: "64px",
        96: "96px",
        128: "128px",
      },
      boxShadow: {
        "negative-y": "0 -8px 16px -3px rgba(0, 0, 0, 0.15)",
      },
      maxWidth: {
        lg: "33rem",
        "2xl": "40rem",
        "3xl": "50rem",
        "5xl": "66rem",
      },
      maxHeight: {
        128: "32rem",
        160: "40rem",
        192: "48rem",
      },
      spacing: {
        128: "32rem",
        128: "32rem",
        160: "40rem",
        192: "48rem",
      },
      ringWidth: ["hover"],
      typography: (theme) => ({
        DEFAULT: {
          css: {
            color: theme("colors.slate.light.800"),
            "prose a": {
              color: "inherit", // Reset color
              textDecoration: "inherit", // Reset text decoration
              backgroundColor: "transparent", // Reset background color
              borderColor: "transparent", // Reset border color
            },
            a: {
              color: theme("colors.blue.light.600"), // Normal state
              textDecoration: "none", // Remove underline by default
              fontWeight: "500", // Not bold
              transition: "color 0.3s ease-in-out", // Animates the color and borderColor change
              "&:hover": {
                color: theme("colors.blue.light.400"), // Hover state color
              },
            },
            "li::marker": { color: theme("colors.slate.light.500") },
            code: { color: theme("colors.coral") },
            "h2 code": { color: theme("colors.coral") }, // Because css specificity is a PITA
            "h3 code": { color: theme("colors.coral") },
            "h4 code": { color: theme("colors.coral") },
            "h5 code": { color: theme("colors.coral") },
            "h6 code": { color: theme("colors.coral") }, // Because css specificity is a PITA
            "code::before": { content: "none" },
            "code::after": { content: "none" },
            img: { width: theme("width.full") },
          },
        },
        dark: {
          css: {
            color: theme("colors.light.dark.100"),
            strong: { color: theme("colors.light.dark.100") },
            a: {
              color: theme("colors.blue.dark.400"), // Dark mode color
              "&:hover": {
                color: theme("colors.blue.dark.600"), // Dark mode hover color
              },
            },
            hr: { borderColor: theme("colors.slate.dark.500") },
            "li::marker": { color: theme("colors.slate.dark.200") },
          },
        },
      }),
    },
    // Extend translateY utilities for negative values if they don't exist
    translate: {
      "-10": "-2.5rem", // Example value, adjust as needed for your design
      // Add more negative values as necessary for different distances
    },
    // Ensure transition-property utilities include translate if not already
    transitionProperty: {
      height: "height, max-height",
      spacing: "margin, padding",
      translate: "transform", // Ensures translate transformations are smoothly transitioned
    },
    // Add 'transitionColor' to the 'extend' object
    transitionColor: ["responsive", "hover", "focus"],
    // Extend duration if needed for longer transitions
    transitionDuration: {
      200: "200ms",
      300: "300ms",
      400: "400ms",
      500: "500ms",
    },
  },
  variants: {
    extend: {
      typography: ["dark"], // add 'dark' variant
      translate: ["responsive", "hover", "focus", "active", "group-hover"],
      transitionProperty: ["responsive", "hover", "focus"],
    },
  },
  plugins: [
    require("@tailwindcss/typography"),
  ],
};




    