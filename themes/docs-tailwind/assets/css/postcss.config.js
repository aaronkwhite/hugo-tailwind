const themeDir = __dirname + "/../../";

module.exports = {
  plugins: [
    require("postcss-import"),
    require('postcss-url'),
    require("tailwindcss")(themeDir + "assets/css/tailwind.config.js"),
    require("autoprefixer")({
      path: [themeDir],
    }),
    //...(process.env.HUGO_ENVIRONMENT === "production" ? [purgecss] : []),
  ],
};

