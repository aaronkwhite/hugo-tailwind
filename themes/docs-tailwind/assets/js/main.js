
// Theme switcher
document.addEventListener("DOMContentLoaded", () => {
  const themeToggleBtn = document.getElementById("theme-toggle");
  const themeToggleDarkIcon = document.getElementById("theme-toggle-dark-icon");
  const themeToggleLightIcon = document.getElementById(
    "theme-toggle-light-icon"
  );

  const isDarkMode = () => document.documentElement.classList.contains("dark");
  const prefersDarkScheme = window.matchMedia(
    "(prefers-color-scheme: dark)"
  ).matches;

  const setMode = (mode) => {
    document.documentElement.classList.remove("light", "dark");
    document.documentElement.classList.add(mode);
    localStorage.setItem("color-theme", mode);
    toggleIcons(); // Ensure icons are toggled right after setting the mode
  };

  const toggleIcons = () => {
    if (isDarkMode()) {
      themeToggleDarkIcon.style.display = "none";
      themeToggleLightIcon.style.display = "block";
    } else {
      themeToggleDarkIcon.style.display = "block";
      themeToggleLightIcon.style.display = "none";
    }
  };

  // Determine initial theme based on user's preference or local storage
  if (localStorage.getItem("color-theme")) {
    // If there's a theme set in localStorage
    setMode(localStorage.getItem("color-theme"));
  } else if (prefersDarkScheme) {
    // If user prefers dark scheme according to their system settings
    setMode("dark");
  } else {
    // Default to light mode
    setMode("light");
  }

  themeToggleBtn.addEventListener("click", () => {
    // Toggle between dark and light mode on button click
    if (isDarkMode()) {
      setMode("light");
    } else {
      setMode("dark");
    }
  });
});

// Show sidebar on mobile
document.addEventListener("DOMContentLoaded", () => {
  const sidebarToggle = document.getElementById("sidebarToggle");
  const hamburgerIcon = document.getElementById("hamburgerIcon");
  const closeIcon = document.getElementById("closeIcon");
  const sidebarNav = document.getElementById("sidebarNav");
  const sidebar = document.getElementById("sidebar");

  sidebarToggle.addEventListener("click", toggleSidebar);

  function toggleSidebar() {
    sidebar.classList.toggle("hidden");
    sidebarNav.classList.toggle("hidden");

    // Toggle visibility of icons
    if (hamburgerIcon.classList.contains("hidden")) {
      // If hamburger is hidden, show it and hide close icon
      hamburgerIcon.classList.remove("hidden");
      closeIcon.classList.add("hidden");
    } else {
      // If hamburger is shown, hide it and show close icon
      hamburgerIcon.classList.add("hidden");
      closeIcon.classList.remove("hidden");
    }

    ensureActiveSubmenuVisible();
  }

  function ensureActiveSubmenuVisible() {
    const activeSubmenuItems = document.querySelectorAll(
      ".submenu .submenu-item.active"
    );
    activeSubmenuItems.forEach(expandSubmenu);
  }

  function expandSubmenu(item) {
    const parentSubmenu = item.closest(".submenu");
    if (parentSubmenu) {
      removeHiddenClasses(parentSubmenu);
      expandParentMenus(parentSubmenu);
    }
  }

  function removeHiddenClasses(element) {
    element.classList.remove("hidden", "opacity-0", "max-h-0");
    element.style.maxHeight = "none";
  }

  function expandParentMenus(submenu) {
    let parent = submenu.parentElement.closest(".submenu");
    while (parent) {
      removeHiddenClasses(parent);
      parent = parent.parentElement.closest(".submenu");
    }
  }
});

// Sidebar show/hide animations
document.addEventListener("DOMContentLoaded", function () {
  const menuButtons = document.querySelectorAll(".menu-btn");

  menuButtons.forEach((button) => {
    const submenu = button.nextElementSibling;
    const svg = button.querySelector(".menu-icon");

    // Check if the submenu should be automatically shown
    const hasActiveItem =
      submenu.querySelector(".submenu-item.active") !== null;
    if (hasActiveItem) {
      toggleSubmenu(submenu, svg, true);
    }

    button.addEventListener("click", () => {
      const shouldShow = submenu.classList.contains("opacity-0");
      toggleSubmenu(submenu, svg, shouldShow);
    });
  });

  function toggleSubmenu(submenu, svg, shouldShow) {
    if (shouldShow) {
      showSubmenu(submenu, svg);
    } else {
      hideSubmenu(submenu, svg);
    }
  }

  function showSubmenu(submenu, svg) {
    submenu.classList.remove("hidden", "opacity-0", "scale-95");
    submenu.style.maxHeight = `${submenu.scrollHeight}px`;
    animateSubmenuItems(submenu, true);
    rotateSVG(svg, 90);
  }

  function hideSubmenu(submenu, svg) {
    submenu.classList.add("opacity-0", "scale-95");
    submenu.style.maxHeight = "0";
    animateSubmenuItems(submenu, false);
    rotateSVG(svg, 0);
  }

  function animateSubmenuItems(submenu, shouldShow) {
    const items = submenu.querySelectorAll(".submenu-item");
    items.forEach((item, idx) => {
      if (shouldShow) {
        setTimeout(() => {
          item.classList.remove("opacity-0", "translate-y-1");
          item.classList.add("translate-y-0");
        }, 20 * idx); // Staggered effect
      } else {
        item.classList.add("opacity-0", "translate-y-1");
        item.classList.remove("translate-y-0");
      }
    });
  }

  function rotateSVG(svg, degrees) {
    if (svg) {
      svg.style.transform = `rotate(${degrees}deg)`;
    }
  }
});

// change the id of the toc-mini because it conflicts with the toc
document.addEventListener("DOMContentLoaded", function () {
  var nav = document.querySelector("#toc-mini > nav");
  if (nav) {
    nav.id = "toc-mini-nav";

  }
});

// TOC Scroll Spy
document.addEventListener("DOMContentLoaded", () => {
  const tocLinks = Array.from(document.querySelectorAll("#TableOfContents a"));
  const sections = tocLinks.map((link) => {
    const sectionId = link.getAttribute("href").substring(1);
    const section = document.getElementById(sectionId);
    link.classList.add("toc-item");
    return { link, section };
  });

  let ignoreScrollEvent = false;

  sections.forEach(({ link, section }) => {
    link.addEventListener("click", (e) => {
      e.preventDefault();
      ignoreScrollEvent = true;
      setActiveLink(link);
      section.scrollIntoView({ behavior: "smooth" });
      setTimeout(() => {
        ignoreScrollEvent = false;
      }, 1000);
    });
  });

  window.addEventListener("scroll", () => {
    if (!ignoreScrollEvent) {
      const current = getCurrentAnchor(sections, window.pageYOffset);
      if (current) {
        setActiveLink(current.link);
      }
    }
  });

  // Manually invoke the scroll handling logic on page load to set the initial active link
  handleInitialActiveLink();

  function handleInitialActiveLink() {
    const current = getCurrentAnchor(sections, window.pageYOffset);
    if (current) {
      setActiveLink(current.link);
    } else {
      // Ensure sections[0] exists before attempting to access its link property
      if (sections[0]) {
        setActiveLink(sections[0].link);
      }
    }
  }
});

function setActiveLink(link) {
  document
    .querySelectorAll("#TableOfContents a.active")
    .forEach((activeLink) => {
      activeLink.classList.remove("active");
    });
  link.classList.add("active");
}

function getCurrentAnchor(sections, winY) {
  const winH = window.innerHeight;
  let closestSection = null;
  let closestSectionDist = Infinity;

  sections.forEach(({ link, section }) => {
    const sectionY = section.getBoundingClientRect().top + winY - 50;
    const distance = Math.abs(winY - sectionY);

    if (distance < closestSectionDist) {
      closestSectionDist = distance;
      closestSection = { link, section };
    }
  });

  const nearBottom =
    window.innerHeight + window.scrollY >= document.body.offsetHeight - 10;
  if (nearBottom) {
    return {
      link: sections[sections.length - 1].link,
      section: sections[sections.length - 1].section,
    };
  }

  return closestSection;
}

// Copy code block to clipboard
document.addEventListener("DOMContentLoaded", function () {
  document.querySelectorAll(".copy-button").forEach((button) => {
    button.addEventListener("click", function () {
      const codeBlock = button.previousElementSibling;
      const code = codeBlock ? codeBlock.textContent : "";

      navigator.clipboard.writeText(code).then(() => {
        const copySpan = button.querySelector('span[aria-hidden="false"]');
        const copiedSpan = button.querySelector('span[aria-hidden="true"]');

        // Add transition and background/border color classes for the "Copied!" state
        button.classList.add(
          "transition-colors",
          "duration-300",
          "ease-in-out"
        );
        button.classList.add("bg-blue-light-400/20", "border-blue-light-400");

        // Check for dark mode and adjust the border color
        if (document.documentElement.classList.contains("dark")) {
          button.classList.add("dark:border-blue-dark-500");
          button.classList.remove("dark:bg-white/2.5", "dark:hover:bg-white/5");
        } else {
          button.classList.remove("dark:border-blue-dark-500");
        }

        button.classList.remove(
          "bg-white/5",
          "hover:bg-white-light/7.5",
          "dark:bg-white-light/2.5",
          "dark:hover:bg-white-light/5"
        );

        // Show the "Copied!" span
        copiedSpan.style.opacity = "1";
        copiedSpan.style.transform = "translate-y-0";

        // Hide the "Copy" span
        copySpan.style.opacity = "0";

        setTimeout(() => {
          // Revert button classes back to initial state after 2 seconds
          button.classList.remove(
            "bg-blue-light-400/20",
            "border-blue-light-400",
            "dark:border-blue-dark-500"
          );
          button.classList.add(
            "bg-white/5",
            "hover:bg-white-light/7.5",
            "dark:bg-white-light/2.5",
            "dark:hover:bg-white-light/5"
          );

          // Show the "Copy" span
          copySpan.style.opacity = "1";

          // Hide the "Copied!" span
          copiedSpan.style.opacity = "0";
          copiedSpan.style.transform = "translate-y-1.5";
        }, 2000);
      });
    });
  });
});

// Show / Hide more codeblock
document.addEventListener("DOMContentLoaded", function () {
  document.querySelectorAll(".code-block-container").forEach((container) => {
    const lines = container.innerText.split("\n").length;
    const showMoreCntr = container.parentElement.querySelector(
      ".show-more-container"
    );
    const showMoreBtn = container.parentElement.querySelector(".show-more-btn");
    const arrowDown =
      '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" class="h-5 w-5 inline-block"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 4v16m0 0l-8-8m8 8l8-8"></path></svg>';
    const arrowUp =
      '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" class="h-5 w-5 inline-block"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 20v-16m0 0l-8 8m8-8l8 8"></path></svg>';

    // Check the number of lines in the code block
    // If it's more than 40, add the "Show More" button
    if (lines > 40) {
      container.classList.add("max-h-24");
      showMoreCntr.classList.add("shadow-negative-y");
      showMoreBtn.classList.remove("hidden");

      // Initial Down arrow for "Show More"
      showMoreBtn.innerHTML = arrowDown;

      showMoreBtn.addEventListener("click", function () {
        if (container.style.maxHeight !== "none") {
          // Expand the container
          container.style.maxHeight = "none";

          // Up arrow for "Show Less"
          showMoreCntr.classList.remove("shadow-negative-y");
          showMoreBtn.innerHTML = arrowUp;
        } else {
          // Collapse the container
          container.style.maxHeight = "48rem"; // Adjust this value as needed

          // Down arrow for "Show More"
          showMoreCntr.classList.add("shadow-negative-y");
          showMoreBtn.innerHTML = arrowDown;
        }
      });
    }
  });
});

// Anchor links, requires 'enableAnchors: true' in front matter
window.onload = function() {
  var enableAnchors = document.querySelector('body').classList.contains('enable-anchors');
  if (enableAnchors) {
    var mainElement = document.querySelector('main');
    var headings = mainElement.querySelectorAll('h2, h3, h4, h5, h6');
    Array.prototype.forEach.call(headings, function(heading) {
      var link = document.createElement('a');
      link.href = '#' + heading.id;
      link.innerHTML = '<span class="h-copy ml-2 text-slate-light-400 dark:text-slate-dark-300 h-2 w-2"><i class="fad fa-copy"></i></span>';
      link.style.display = 'none';
      heading.appendChild(link);
      heading.onmouseover = function() {
        link.style.display = 'inline';
      };
      heading.onmouseout = function() {
        link.style.display = 'none';
      };
      link.onclick = function(event) {
        event.preventDefault();
        var text = window.location.href.split('#')[0] + link.hash;
        navigator.clipboard.writeText(text);
      };
    });
  }
};